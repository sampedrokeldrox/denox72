#!/bin/bash

# Obtenemos nombre del usuario
read -p "Directorio donde se encuentra el proyecto del servidor remoto: " sPathRemote
read -p "Puerto SSH: " sshPort
read -p "Usuario SSH: " sshUser
read -p "Server SSH: " sshServer
read -p "Server SSH: " aExclude

# Obtenemos su basename(nombre del proyecto)
sBaseName=$(basename $sPathRemote)

# Comprobamos SSH
status=$(ssh -o BatchMode=yes -o ConnectTimeout=5 $sshUser@$sshServer -p $sshPort echo ok 2>&1)

# Si todo es correcto
if [[ $status == ok ]] ; then
	# Creamos el directorio donde se montara
	mkdir -p /media/aux

	# Si esta montado desmontamos par montar el nuevo
	if grep -qs '/media/aux ' /proc/mounts; then
		umount /media/aux
	fi

	# Montamos
	sshfs -p $sshPort -C $sshUser@$sshServer:$sPathRemote /media/aux/

	# Obtenemos configuracion mysql
	sDbServer=$(php -r 'include("/media/aux/includes/configure.php"); echo DB_SERVER;')
	sDbUser=$(php -r 'include("/media/aux/includes/configure.php"); echo DB_SERVER_USERNAME;')
	sDbPassword=$(php -r 'include("/media/aux/includes/configure.php"); echo DB_SERVER_PASSWORD;')
	sDbDatabase=$(php -r 'include("/media/aux/includes/configure.php"); echo DB_DATABASE;')

	# Realizamos backup
	echo "Exportando base de datos..."
	ssh $sshUser@$sshServer -p $sshPort "mysqldump --user=$sDbUser --password=$sDbPassword --host=$sDbServer $sDbDatabase --default-character-set=utf8 > $sPathRemote/vagrant_backup.sql"

	# Comprimimos
	ssh $sshUser@$sshServer -p $sshPort tar -czf $sPathRemote/vagrant_backup.tar.gz $sPathRemote/vagrant_backup.sql

	# Copiamos
	rsync -avh --progress /media/aux/vagrant_backup.tar.gz ./

	# Eliminamos
	rm -rf /media/aux/vagrant_backup.tar.gz
	rm -rf /media/aux/vagrant_backup.sql

	# Descomprimimos
	tar -zxvf ./vagrant_backup.tar.gz

	# Movemos
	mv ./$sPathRemote/{.,}* ./ &> /dev/null

	# Eliminamos
	rm -rf ./home
	rm -rf ./vagrant_backup.tar.gz

	# Desmontamos
	umount /media/aux

	# Creamos base de datos
	echo "Importando base de datos..."
	mysql -udenox -pdenox -e "DROP DATABASE IF EXISTS $sDbDatabase;"
	mysql -udenox -pdenox -e "CREATE DATABASE $sDbDatabase;"
	mysql -f -udenox -pdenox $sDbDatabase < vagrant_backup.sql

	# Eliminamos
	rm -rf ./vagrant_backup.sql

	echo "Base de datos importada..."
elif [[ $status == "Permission denied"* ]] ; then
	echo "No tienes permisos, comprueba la llave publica en el servidor"
else
	echo "Imposible conectar, compruebe el puerto de conexión, servidor, usuario u otro problema"
fi