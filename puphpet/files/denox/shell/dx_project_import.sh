#!/bin/bash

# Variable
sExclude=""

# Obtenemos nombre del usuario
read -p "Directorio donde se encuentra el proyecto del servidor remoto: " sPathRemote
read -p "Puerto SSH: " sshPort
read -p "Usuario SSH: " sshUser
read -p "Server SSH: " sshServer
read -p "Server SSH: " aExclude

# Obtenemos su basename(nombre del proyecto)
sBaseName=$(basename $sPathRemote)

# Comprobamos si existe el nombre del proyecto
if test -d ./$sBaseName; then
	echo "El proyecto $sBaseName existe no se realizara nada, compruebe lo necesario y vuelve a ejecutar"
	exit
fi 

# Los directorios/archivos exluidos
IFS=' ' read -ra ADDR <<< "$aExclude"
for i in "${ADDR[@]}"; do
    sExclude=$sExclude' --exclude='\'$sPathRemote/$i\'
done

# Comprobamos SSH
status=$(ssh -o BatchMode=yes -o ConnectTimeout=5 $sshUser@$sshServer -p $sshPort echo ok 2>&1)

# Si todo es correcto
if [[ $status == ok ]] ; then
	# Realizamos backup
	ssh $sshUser@$sshServer -p $sshPort tar -vpczf $sPathRemote/vagrant_backup.tar.gz $sPathRemote $sExclude

	# Creamos el directorio donde se montara
	mkdir -p /media/aux

	# Si esta montado desmontamos par montar el nuevo
	if grep -qs '/media/aux ' /proc/mounts; then
		umount /media/aux
	fi

	# Montamos
	sshfs -p $sshPort -C $sshUser@$sshServer:$sPathRemote /media/aux/

	# Si esta montado
	if grep -qs '/media/aux ' /proc/mounts; then	
		# Copiamos
		rsync -avh --progress /media/aux/vagrant_backup.tar.gz ./

		# Eliminamos
		rm -rf /media/aux/vagrant_backup.tar.gz

		# Descomprimimos
		tar -zxvf ./vagrant_backup.tar.gz

		# Creamos directorio
		mkdir ./$sBaseName

		# Movemos
		mv ./$sPathRemote/{.,}* ./$sBaseName &> /dev/null

		# Eliminamos
		rm -rf ./home
		rm -rf ./vagrant_backup.tar.gz

		# Desmontamos
		umount /media/aux
	else
	    echo "El directorio introducido no existe compruebalo"
	fi
elif [[ $status == "Permission denied"* ]] ; then
	echo "No tienes permisos, comprueba la llave publica en el servidor"
else
	echo "Imposible conectar, compruebe el puerto de conexión, servidor, usuario u otro problema"
fi