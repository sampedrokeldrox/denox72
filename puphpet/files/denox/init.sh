#!/bin/bash

cp /var/www/puphpet/files/denox/shell/dx_mysql_import.sh /usr/local/bin
cp /var/www/puphpet/files/denox/shell/dx_project_import.sh /usr/local/bin

curl -LO https://deployer.org/deployer.phar
mv deployer.phar /usr/local/bin/dep
chmod +x /usr/local/bin/dep